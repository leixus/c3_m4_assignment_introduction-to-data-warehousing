# C3_M4_Assignment: Introduction to Data Warehousing
This is the assignment for this module. All works are done through cooperations and thorough discussions between the following group members:
* Xiangyu Lei
* Shuang Zheng

## Tasks
* Design the star-schema of the data warehouse and implement the structure including the following tables:
    * `dim_time`: dimension table, time-related data;
    * `dim_customer`: dimension table, customer-related data;
    * `dim_inventory`: dimension table, inventory_related data;
    * `fact_sales`: fact table, showing various facts of sales

* Extract relevant data from the provided database (coffee_merchant.sql) 

* Transform the extracted data into a format suitable for the data warehouse designed above

* Load the transformed data

## Deliverables

* **report.pdf**

    The report documents the ETL process with screenshots and descriptions of each step. It also includes some reflections during the assignment.

* **star_schema_design.png**

    The picture shows the schema design for the data warehouse, including 3 dimension tables and 1 fact table.

* **all_queries.sql**

    The file includes all queries for this assignment, including data extraction for all dimension and fact tables, transformation process, table creation in the data warehouse.

* **dim_customer_data.csv**
* **dim_inventory_data.csv**
* **dim_time_data.csv**
* **fact_sales_data.csv**

    All these .csv files are resulted and **exported** from extration process, which are used in following **loading** process.