/* EXTRACT process */
-- Select corresponding columns from "consumers" table, and export results for "dim_customer" dimension table later
SELECT "ConsumerID" AS customer_id, CONCAT("FirstName", ' ', "LastName") AS customer_name, "Phone" AS phone, CONCAT("Street", ', ', "City", ', ', "State") AS address, "Zipcode" AS zipcode FROM consumers;

-- Select corresponding columns from "inventory" table, and export results for "dim_inventory" dimension table later
-- Use ABS() function on column "OnHand" to transform negative values to positive
SELECT "InventoryID" AS inventory_id, "Name" AS name, "ItemType" AS item_type, "Description" AS description, ABS("OnHand") AS on_hand, "Price" AS price FROM inventory;

-- Join relevant tables and select corresponding columns, and export results for "fact_sales" fact table later
SELECT o."OrderDate" AS order_date, o."ConsumerID" AS customer_id, ol."InventoryID" AS inventory_id, ol."Quantity" AS quantity, ol."Quantity"*ol."Price"*(1-ol."Discount") AS total_sales
FROM orders AS o
INNER JOIN orderlines AS ol ON o."OrderID" = ol."OrderID";

-- Create data for the "dim_time" dimension table
SELECT 
	full_date::date AS date, -- This is to cast the full timestamp to only date (i.e., without hour-minute-second)
	EXTRACT('YEAR' FROM full_date) AS year,
	EXTRACT('MONTH' FROM full_date) AS month,
	EXTRACT('DAY' FROM full_date) AS day,
	CASE 
		WHEN day_of_week = 0 THEN 'Sunday'
		WHEN day_of_week = 1 THEN 'Monday'
		WHEN day_of_week = 2 THEN 'Tuesday'
		WHEN day_of_week = 3 THEN 'Wednesday'
		WHEN day_of_week = 4 THEN 'Thursday'
		WHEN day_of_week = 5 THEN 'Friday'
		WHEN day_of_week = 6 THEN 'Saturday'
	END AS day_of_week
FROM(
	SELECT full_date, EXTRACT(dow FROM full_date) AS day_of_week
	FROM(
		SELECT generate_series('2005-01-01'::timestamp, '2006-12-31'::timestamp, '1 day'::interval) AS full_date
	)
);


/* Create dimension tables and fact tables based on designed warehouse star-schema (see file: star_schema_design.png) */
CREATE TABLE dim_time(
	"date" date PRIMARY KEY,
	"year" INT,
	"month" INT,
	"day" INT,
	day_of_week TEXT
);

CREATE TABLE dim_customer(
	customer_id INT PRIMARY KEY,
	customer_name VARCHAR(60) NOT NULL,
	phone VARCHAR(20),
	address VARCHAR(150),
	zipcode VARCHAR(5)
);

CREATE TABLE dim_inventory(
	product_id INT PRIMARY KEY,
	product_name VARCHAR(40) NOT NULL,
	category VARCHAR(1) NOT NULL,
	product_description VARCHAR(500),
	on_hand INT NOT NULL CHECK(on_hand >= 0),
	price DECIMAL NOT NULL CHECK(price > 0)	
);

CREATE TABLE fact_sales(
	sales_id SERIAL PRIMARY KEY,
	"date" date REFERENCES dim_time(date),
	customer_id INT REFERENCES dim_customer(customer_id),
	product_id INT REFERENCES dim_inventory(product_id),
	quantity_sold INT CHECK(quantity_sold >= 0),
	total_sales DECIMAL CHECK(total_sales >= 0)
);
